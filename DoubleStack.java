/**
 * @author D-Abraham
 */
public class DoubleStack{

  private String[] array;
  private int pointer1;
  private int pointer2;

  public DoubleStack(int arraySize){
    array = new String[arraySize];
    pointer1 = 0;
    pointer2 = arraySize-1;
  }

  public boolean empty(int stackNum){
    if(stackNum == 1){
      if(array[pointer1] != null){
        return false;
      }
    }
    if(stackNum == 2){
      if(array[pointer2] != null){
        return false;
      }
    }
    return true;
  }

  public String push(String item, int stackNum){
    if(pointer1 < pointer2){
      if(stackNum == 1){
        array[pointer1] = item;
        pointer1++;
        return item;
      }
      else if(stackNum == 2){
        array[pointer2] = item;
        pointer2--;
        return item;
      }
    }
    else if (pointer1 == pointer2){
      if(stackNum == 1){
        array[pointer1] = item;
        pointer1++;
        return item;
      }
      else if(stackNum == 2){
        array[pointer2] = item;
        pointer2--;
        return item;
      }
    }

    return null;
  }

  public String pop(int stackNum){
    String temp="";
    if(stackNum == 1){
      temp = array[pointer1-1];
      array[pointer1-1]=null;
      pointer1--;
      return temp;
    }
    else if(stackNum == 2){
      temp = array[pointer2+1];
      array[pointer2+1]=null;
      pointer2++;
      return temp;
    }
    return null;
  }

  public String peek(int stackNum){
    if(stackNum == 1){
      return array[pointer1-1];
    }
    else if (stackNum == 2) {
      return array[pointer2+1];
    }
    return null;
  }

  public String toString(){
    String temp = "";
    for(String x: array){
      temp = temp + x +"\t";
    }
    return temp;
  }

}
