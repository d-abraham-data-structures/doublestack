/**
 * @author D-Abraham
 */
public class Test{
  public static void main(String[] args){

    DoubleStack stack = new DoubleStack(10);
    System.out.println("Is empte (1,2): " + stack.empty(1)+", "+stack.empty(2));
    System.out.println("adding to both stacks.");
    for (int i=0;i<5 ;i++ ){
      stack.push("Exp"+i, 1);
      stack.push("Exp"+i, 2);
    }
    System.out.println(stack.toString());

    System.out.println("Is empty 1,2 (false): "+ stack.empty(1)+", "+ stack.empty(2));
    System.out.println("Peek:" + stack.peek(1) +", "+ stack.peek(2) );
    System.out.println(stack.toString());
    System.out.println("Push while full (null):"+ stack.push("item", 1)+", "+
    stack.push("item", 2));
    System.out.println("After push while full>> "+stack.toString());
    System.out.println("Pop: "+ stack.pop(1)+", "+stack.pop(2));
    System.out.println(stack.toString());
    System.out.println("Peek:" + stack.peek(1) +", "+ stack.peek(2) );

  }
}
